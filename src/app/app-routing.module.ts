import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './Pages/Login/login.component';
import { LayoutComponent } from './Pages/Shared/Layout/layout.component'
import { Auth } from './Services/Guard/guard.service'
import { DashBoardComponent } from './Pages/DashBoard/dash-board.component';
import { AdminAreaComponent } from './Pages/AdminArea/admin-area.component';
import { ShipperComponent } from './Pages/AdminArea/Shipper/shipper.component'

import { UdmMasterComponent } from './Pages/AdminArea/UDMMaster/udm-master.component'
import { VehiclesComponent } from './Pages/AdminArea/Vehicles/vehicles.component'
import { NatureOfGoodsComponent } from './Pages/AdminArea/NatureofGoods/nature-of-goods.component'
import { ExportComponent } from './Pages/Export/export.component'
import { AcceptanceComponent } from './Pages/Export/Acceptance/acceptance.component'
import { NoticeTypesComponent } from './Pages/AdminArea/NoticeTypes/notice-types.component'
import { ExaminationComponent } from './Pages/Export/Examination/examination.component'
import { ScanningComponent } from './Pages/Export/Scanning/scanning.component'
import { ULDTypesComponent } from './Pages/AdminArea/ULDTypes/uldtypes.component'
import { ULDRouteComponent } from './Pages/ULD/uldroute.component'
import { ULDComponent } from './Pages/ULD/ULD/uld.component'
import { ULDReceiptComponent } from './Pages/ULD/ULDReceipt/uldreceipt.component'
import { ReveiveULDInquiryComponent } from './Pages/ULD/ULDReceiveInquiry/reveive-uldinquiry.component';
import { FlightsComponent } from './Pages/Export/Flights/flights.component';

import { UldStockEachComponent } from './Pages/ULD/ULDStock/uldStockEach/uld-stock-each.component';
import { UldStockAllComponent } from './Pages/ULD/ULDStock/uldStockAll/uld-stock-all.component';
import { EmailComponent } from './email/email.component';
import { GroupRolesComponent } from './Pages/AdminArea/Roles/GroupRoles/group-roles.component'
import { ChangePasswordComponent } from './Pages/UserProfile/ChangePassword/change-password.component'
import { EmailModuleComponent } from './email-module/email-module.component';
import { NotifyComponent } from './Pages/Notify/notify/notify.component';
import { DeptModuleComponent } from './Pages/DeptModule/dept-module.component';
import { AssignGroupRolesComponent } from './Pages/AdminArea/Assign-Group-Roles/assign-group-roles.component';
import { SurveyComponent } from './Pages/AdminArea/survey/survey.component';
import { StatementsComponent } from './Pages/AdminArea/statements/statements.component';
import { AssignRolesComponent } from './Pages/AdminArea/Assign-Roles/assign-roles.component';
import { HRPageComponent } from './Pages/AdminArea/HR-Page/hr-page.component';

const routes: Routes = [

  {
    path: '', component: LayoutComponent, canActivate: [Auth], children: [
      { path: 'Dashboard', component: DashBoardComponent },
      {
        path: 'Admin', component: AdminAreaComponent, children: [
          { path: 'AssignRoles', component: AssignRolesComponent },
          { path: 'AssignGroupRoles', component: AssignGroupRolesComponent },
          { path: 'Surveys', component: SurveyComponent },
          { path: 'Statements', component: StatementsComponent },
          { path: 'HRPage', component: HRPageComponent },
          

          { path: 'Shipper', component: ShipperComponent },
          { path: 'GroupRoles', component: GroupRolesComponent },
          { path: 'UDM Master', component: UdmMasterComponent },
          { path: 'Vehicles', component: VehiclesComponent },
          { path: 'NatureOfGoods', component: NatureOfGoodsComponent },
          { path: 'NoticeType', component: NoticeTypesComponent },
          { path: 'ULDTypes', component: ULDTypesComponent },
          { path: 'Email', component: EmailComponent },
          { path: 'EmailModule', component: EmailModuleComponent },
          { path: 'Notify', component: NotifyComponent },
        ]
      },
      {
        path: 'Export', component: ExportComponent, children: [
          { path: 'Acceptance', component: AcceptanceComponent },
          { path: 'Flights', component: FlightsComponent },
          { path: 'Examination', component: ExaminationComponent },
          { path: 'Scanning', component: ScanningComponent },
          { path: 'DepartModule', component: DeptModuleComponent },
        ]
      },
      {
        path: 'ULD', component: ULDRouteComponent, children: [
          { path: 'ULD', component: ULDComponent },
          { path: 'ULDReceive', component: ULDReceiptComponent },
          { path: 'ULDReceive/:id', component: ULDReceiptComponent },
          { path: 'ULDReceiveInquiry', component: ReveiveULDInquiryComponent },
          { path: 'UldStockEachComponent', component: UldStockEachComponent },
          { path: 'UldStockAllComponent', component: UldStockAllComponent },
        ]
      },
    ]
  },
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'changePassword', component: ChangePasswordComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
